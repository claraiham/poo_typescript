type Address = {
    street: string,
    city: string,
    postalCode: string,
    country: string
}

class Customers {
    //déclaration des propriétés et leur type
    // public pour rendre accessible les propriétés
    public customerId: number;
    public name: string;
    public email: string;
    public address: Address | undefined;

    constructor(customerId: number, name: string, email: string) {
        //déclaration des variables liées aux propriétés
        this.customerId = customerId,
            this.name = name,
            this.email = email;
    }

    /**
     * displayInfo : permet de retourner une chaine de caracteres contenant l'id, le nom et l'email de l'utilisateur
     */
    public displayInfo(): string {//méthode
        return (
            `Customer ID: ${this.customerId}
        Name: ${this.name} 
        Email: ${this.email}
        ${this.displayAddress()}`
        )
    }

    // public set Address(address: Address) {
    //     // setter Address() pour renseigner l'adresse du client
    //         this.address = address
    // }

    public setAddress(address: Address) {
        return (
            this.address = address
        )
    }

    /**
     * 
     * displayAddress permet d'afficher l'adresse si elle a été renseignée
     */

    public displayAddress(): string {
        // si l'adresse existe, ou "No address found" sinon.
        if (this.address === undefined) {
            return "No address found"
        } else {
            return (
                `Address: ${this.address.street}
                Code postal: ${this.address.postalCode} 
                Ville: ${this.address.city}
                Pays: ${this.address.country}`
            )
        }

    }

}

const exemple = new Customers(1, "clara", "j@gmail.com")

//appeler setter Address sur exemple pour renseigner l'adresse du client
// exemple.Address = {street: "rue", postalCode: "code postal", city:"ville", country: "pays"}
exemple.setAddress({ street: "rue", postalCode: "code postal", city: "ville", country: "pays" })
console.log(exemple.displayInfo())

console.log('\n')

type Dimensions = {
    length: number,
    width: number,
    height: number
}
class Product {

    public productId: number;
    public name: string;
    public weight: number;
    public price: number;
    public dimensions: Dimensions;

    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions
    }

    /**
     * displayDetails : Retourne les détails d'un produit au format "Product ID: $productId, Name: $name, Weight: $weight, Price: $price €"
     */
    public displayDetails(): string {
        return (
            `Product ID: ${this.productId}
    Name: ${this.name}
    Weight: ${this.weight}
    Price: ${this.price} €
    Longueur: ${this.dimensions.length}
    Largeur: ${this.dimensions.width}
    Hauteur: ${this.dimensions.height}`
        )
    }

}

const product1 = new Product(1, "pull", 0.2, 10, { length: 2, width: 4, height: 5 })
console.log(product1.displayDetails())

/**
 * ClothingSize : enum de tailles de vêtements
 */

enum ClothingSize {
    XS = "XS",
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL"
}

/**
 * ShoeSize : enum de tailles de chaussures
*/

enum ShoeSize {
    size36 = 36,
    size37 = 37,
    size38 = 38,
    size39 = 39,
    size40 = 40,
    size41 = 41,
    size42 = 42,
    size43 = 43,
    size44 = 44,
    size45 = 45,
    size46 = 46
}

/**
* Clothing : sous classe de Product pour renseigner la taille du vêtement
*/

class Clothing extends Product {

    public size: ClothingSize;

    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ClothingSize) {
        super(productId, name, weight, price, dimensions)
        this.size = size;
    }

    public displayDetailsWithSize(): string {
        //ajouter size à product
        return (
            `${super.displayDetails()}
            Taille: ${this.size}`
        )
    }
}
let product3 = new Clothing(2, "t-shirt", 0.2, 35, { length: 2, width: 4, height: 5 }, ClothingSize.M)

/** 
 * Shoes : sous classe de Product pour renseigner la taille des chaussures
 */
class Shoes extends Product {

    public size: ShoeSize;

    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ShoeSize) {
        super(productId, name, weight, price, dimensions)
        this.size = size;
    }

    public displayDetailsWithSize(): string {
        //ajouter size à product
        return (
            `${super.displayDetails()}
            Taille: ${this.size}`
        )
    }
}

let product2 = new Shoes(1, "nike", 0.2, 80, { length: 2, width: 4, height: 5 }, 40)



console.log(product2.displayDetailsWithSize())


class Order {
    orderId: number;
    customer: Customers;
    productList: Product[]; // type tableau de Product
    orderDate: Date;
    delivery: Deliverable | undefined;

    constructor(orderId: number, productList: Product[], orderDate: Date, customer: Customers) {

        this.orderId = orderId;
        this.productList = productList;
        this.orderDate = orderDate;
        this.customer = customer;

    }

    public addProduct(product: Product) {
        // ajouter un produit au tableau
        return this.productList.push(product)
    }

    public removeProduct(productId: number) {
        // supprimer un produit du tableau
        delete this.productList[productId]
        return this.productList
    }

    public calculateWeight() {
        let totalWeight = 0
        // Calcule le poids total de la commande.
        for (let eachProduct of this.productList) {

            totalWeight += eachProduct.weight
        }
        return totalWeight
    }

    public calculateTotal() {
        // Calcule le poids total de la commande.
        let totalWeight: number = 0;

        for (let eachProduct of this.productList) {

            totalWeight += eachProduct.weight;
        }
        return totalWeight;
    }

    public displayOrder() {
        //afficher les détails de chaque produit
        let allProductDetails: any = "";

        for (let eachProduct of this.productList) {
            allProductDetails += eachProduct.displayDetails()
        }

        // Affiche les informations de l'utilisateur ( this.customer.displayInfo() )
        // Affiche le cout total ( this.calculateTotal() )
        return `Utilisateur : ${this.customer.displayInfo()} 
        Panier : ${allProductDetails} 
        Poids total :  ${this.calculateTotal()}
        Infos livraison : ${this.displayDeliveryInfo()}`
    }

    // Permet d'assigner un service de livraison à la commande.
    public setDelivery(delivery: Deliverable) {
        return this.delivery = delivery;
    }

    //méthode calculateShippingCosts(): Utilise la méthode calculateShippingFee de l'objet delivery pour calculer les frais de livraison en fonction du poids total de la commande.
    public calculateShippingCosts(){
        return this.delivery?.calculateShippingFee(this.calculateTotal())
    }

    //Utilise la méthode estimateDeliveryTime de l'objet delivery pour estimer le délai de livraison de la commande.
    public estimateDeliveryTime(){
        return this.delivery?.estimateDeliveryTime(this.calculateTotal())
    }

    public displayDeliveryInfo(){ // pour afficher les infos liées à la livraison si elles existent
        if(this.delivery !== undefined){
            return `Estimation délais de livraison : ${this.estimateDeliveryTime()} jours.
            Frais de livraison : ${this.calculateShippingCosts()}€`
        }else{
            return "Pas de type de livraison choisie."
        }
        
    }
}
//appeler le constructeur Date pour instancier une date
//syntax 0o2 pour le type Date()
const order1 = new Order(1, [], new Date('December 17, 1995 03:24:00'), exemple)


order1.addProduct(product2)
order1.addProduct(product3)

console.log(order1.productList)



interface Deliverable {
    estimateDeliveryTime(weight: number): number;
    calculateShippingFee(weight: number): number;
}

class StandardDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        if (weight < 10) {
            return 7;
        } else {
            return 10;
        }
    }
    calculateShippingFee(weight: number): number {
        if (weight < 1) {
            return 5;
        } else if (weight >= 1 && weight <= 5) {
            return 10;
        } else {
            return 20
        }
    }

}

class ExpressDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        if (weight <= 5) {
            return 1
        } else {
            return 3
        }
    }
    calculateShippingFee(weight: number): number {
        if (weight < 1) {
            return 8;
        } else if (weight >= 1 && weight <= 5) {
            return 14;
        } else {
            return 30;
        }
    }

}
const chronopost = new ExpressDelivery();
const laPoste = new StandardDelivery(); // création d'une instance d'un type de livraison pour l'assigner à une instance de order.
order1.setDelivery(chronopost);// assignation d'une instance de StandardDelivery à order1 ( intance de Order )

console.log(`displayOrder : ${order1.displayOrder()}`);




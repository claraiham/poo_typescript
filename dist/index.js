"use strict";
class Customers {
    constructor(customerId, name, email) {
        //déclaration des variables liées aux propriétés
        this.customerId = customerId,
            this.name = name,
            this.email = email;
    }
    /**
     * displayInfo : permet de retourner une chaine de caracteres contenant l'id, le nom et l'email de l'utilisateur
     */
    displayInfo() {
        return (`Customer ID: ${this.customerId}
        Name: ${this.name} 
        Email: ${this.email}
        ${this.displayAddress()}`);
    }
    // public set Address(address: Address) {
    //     // setter Address() pour renseigner l'adresse du client
    //         this.address = address
    // }
    setAddress(address) {
        return (this.address = address);
    }
    /**
     *
     * displayAddress permet d'afficher l'adresse si elle a été renseignée
     */
    displayAddress() {
        // si l'adresse existe, ou "No address found" sinon.
        if (this.address === undefined) {
            return "No address found";
        }
        else {
            return (`Address: ${this.address.street}
                Code postal: ${this.address.postalCode} 
                Ville: ${this.address.city}
                Pays: ${this.address.country}`);
        }
    }
}
const exemple = new Customers(1, "clara", "j@gmail.com");
//appeler setter Address sur exemple pour renseigner l'adresse du client
// exemple.Address = {street: "rue", postalCode: "code postal", city:"ville", country: "pays"}
exemple.setAddress({ street: "rue", postalCode: "code postal", city: "ville", country: "pays" });
console.log(exemple.displayInfo());
console.log('\n');
class Product {
    constructor(productId, name, weight, price, dimensions) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions;
    }
    /**
     * displayDetails : Retourne les détails d'un produit au format "Product ID: $productId, Name: $name, Weight: $weight, Price: $price €"
     */
    displayDetails() {
        return (`Product ID: ${this.productId}
    Name: ${this.name}
    Weight: ${this.weight}
    Price: ${this.price} €
    Longueur: ${this.dimensions.length}
    Largeur: ${this.dimensions.width}
    Hauteur: ${this.dimensions.height}`);
    }
}
const product1 = new Product(1, "pull", 0.2, 10, { length: 2, width: 4, height: 5 });
console.log(product1.displayDetails());
/**
 * ClothingSize : enum de tailles de vêtements
 */
var ClothingSize;
(function (ClothingSize) {
    ClothingSize["XS"] = "XS";
    ClothingSize["S"] = "S";
    ClothingSize["M"] = "M";
    ClothingSize["L"] = "L";
    ClothingSize["XL"] = "XL";
    ClothingSize["XXL"] = "XXL";
})(ClothingSize || (ClothingSize = {}));
/**
 * ShoeSize : enum de tailles de chaussures
*/
var ShoeSize;
(function (ShoeSize) {
    ShoeSize[ShoeSize["size36"] = 36] = "size36";
    ShoeSize[ShoeSize["size37"] = 37] = "size37";
    ShoeSize[ShoeSize["size38"] = 38] = "size38";
    ShoeSize[ShoeSize["size39"] = 39] = "size39";
    ShoeSize[ShoeSize["size40"] = 40] = "size40";
    ShoeSize[ShoeSize["size41"] = 41] = "size41";
    ShoeSize[ShoeSize["size42"] = 42] = "size42";
    ShoeSize[ShoeSize["size43"] = 43] = "size43";
    ShoeSize[ShoeSize["size44"] = 44] = "size44";
    ShoeSize[ShoeSize["size45"] = 45] = "size45";
    ShoeSize[ShoeSize["size46"] = 46] = "size46";
})(ShoeSize || (ShoeSize = {}));
/**
* Clothing : sous classe de Product pour renseigner la taille du vêtement
*/
class Clothing extends Product {
    constructor(productId, name, weight, price, dimensions, size) {
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }
    displayDetailsWithSize() {
        //ajouter size à product
        return (`${super.displayDetails()}
            Taille: ${this.size}`);
    }
}
let product3 = new Clothing(2, "t-shirt", 0.2, 35, { length: 2, width: 4, height: 5 }, ClothingSize.M);
/**
 * Shoes : sous classe de Product pour renseigner la taille des chaussures
 */
class Shoes extends Product {
    constructor(productId, name, weight, price, dimensions, size) {
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }
    displayDetailsWithSize() {
        //ajouter size à product
        return (`${super.displayDetails()}
            Taille: ${this.size}`);
    }
}
let product2 = new Shoes(1, "nike", 0.2, 80, { length: 2, width: 4, height: 5 }, 40);
console.log(product2.displayDetailsWithSize());
class Order {
    constructor(orderId, productList, orderDate, customer) {
        this.orderId = orderId;
        this.productList = productList;
        this.orderDate = orderDate;
        this.customer = customer;
    }
    addProduct(product) {
        // ajouter un produit au tableau
        return this.productList.push(product);
    }
    removeProduct(productId) {
        // supprimer un produit du tableau
        delete this.productList[productId];
        return this.productList;
    }
    calculateWeight() {
        let totalWeight = 0;
        // Calcule le poids total de la commande.
        for (let eachProduct of this.productList) {
            totalWeight += eachProduct.weight;
        }
        return totalWeight;
    }
    calculateTotal() {
        // Calcule le poids total de la commande.
        let totalWeight = 0;
        for (let eachProduct of this.productList) {
            totalWeight += eachProduct.weight;
        }
        return totalWeight;
    }
    displayOrder() {
        //afficher les détails de chaque produit
        let allProductDetails = "";
        for (let eachProduct of this.productList) {
            allProductDetails += eachProduct.displayDetails();
        }
        // Affiche les informations de l'utilisateur ( this.customer.displayInfo() )
        // Affiche le cout total ( this.calculateTotal() )
        return `Utilisateur : ${this.customer.displayInfo()} 
        Panier : ${allProductDetails} 
        Poids total :  ${this.calculateTotal()}
        Infos livraison : ${this.displayDeliveryInfo()}`;
    }
    // Permet d'assigner un service de livraison à la commande.
    setDelivery(delivery) {
        return this.delivery = delivery;
    }
    //méthode calculateShippingCosts(): Utilise la méthode calculateShippingFee de l'objet delivery pour calculer les frais de livraison en fonction du poids total de la commande.
    calculateShippingCosts() {
        var _a;
        return (_a = this.delivery) === null || _a === void 0 ? void 0 : _a.calculateShippingFee(this.calculateTotal());
    }
    //Utilise la méthode estimateDeliveryTime de l'objet delivery pour estimer le délai de livraison de la commande.
    estimateDeliveryTime() {
        var _a;
        return (_a = this.delivery) === null || _a === void 0 ? void 0 : _a.estimateDeliveryTime(this.calculateTotal());
    }
    displayDeliveryInfo() {
        if (this.delivery !== undefined) {
            return `Estimation délais de livraison : ${this.estimateDeliveryTime()} jours.
            Frais de livraison : ${this.calculateShippingCosts()}€`;
        }
        else {
            return "Pas de type de livraison choisie.";
        }
    }
}
//appeler le constructeur Date pour instancier une date
//syntax 0o2 pour le type Date()
const order1 = new Order(1, [], new Date('December 17, 1995 03:24:00'), exemple);
order1.addProduct(product2);
order1.addProduct(product3);
console.log(order1.productList);
class StandardDelivery {
    estimateDeliveryTime(weight) {
        if (weight < 10) {
            return 7;
        }
        else {
            return 10;
        }
    }
    calculateShippingFee(weight) {
        if (weight < 1) {
            return 5;
        }
        else if (weight >= 1 && weight <= 5) {
            return 10;
        }
        else {
            return 20;
        }
    }
}
class ExpressDelivery {
    estimateDeliveryTime(weight) {
        if (weight <= 5) {
            return 1;
        }
        else {
            return 3;
        }
    }
    calculateShippingFee(weight) {
        if (weight < 1) {
            return 8;
        }
        else if (weight >= 1 && weight <= 5) {
            return 14;
        }
        else {
            return 30;
        }
    }
}
const chronopost = new ExpressDelivery();
const laPoste = new StandardDelivery(); // création d'une instance d'un type de livraison pour l'assigner à une instance de order.
order1.setDelivery(chronopost); // assignation d'une instance de StandardDelivery à order1 ( intance de Order )
console.log(`displayOrder : ${order1.displayOrder()}`);
